<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Medicatie;
use App\Hond;

class MedicatieController extends Controller
{
  public function store(Request $request) {
    try {

      $medicatie = new Medicatie();
      $medicatie->hondid = Hond::where('familie', '=', Auth::user()->familie)->where('afgemeld', '=', 0)->pluck('id')->first();
      $medicatie->medicatie_naam = $request->input('medicatie_naam');
      $medicatie->medicatie =  $request->input('medicatie');
      $medicatie->tijd =  $request->input('tijd');

      $medicatie->save();

      return redirect('/settings');
    }
    catch(Exception $e) {
      return $e;
      // return redirect('/settings');
    }
  }

  public function update(Request $request, $medicatieid) {
    try {
        $medicatie = Medicatie::where('id', '=', $medicatieid)->update([
          'medicatie_naam' => $request->input('medicatie_naam'),
          'medicatie' => $request->input('medicatie'),
          'tijd' => $request->input('tijd'),
        ]);


        return redirect('/settings');
      }
      catch(Exception $e) {
        // return redirect('/settings');
        return $e;
      }
  }

  public function destroy(Request $request, $medicatieid) {
    try {
      $medicatie = Medicatie::where('id', '=', $medicatieid);

      $medicatie->delete();

      return redirect('/settings');
    }
    catch(Exception $e) {
      // return redirect('/settings');
      return $e;
    }
  }
}
