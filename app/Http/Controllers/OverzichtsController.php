<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uitlaten;
use App\Water;
use App\Voeren;
use App\User;
use Auth;

class OverzichtsController extends Controller
{
    public function show() {
      return view('content.overzichtspagina.show')
      ->with('user', User::all()->where('email','=',Auth::user()->email)->first())
      ->with('riemdetectie', Uitlaten::all())
      ->with('peilConditie', Water::all())
      ->with('gewichtDetectie', Voeren::all());
    }
}
