
@extends('welcome')
@section('content')

<h2>Verify Your Email Adress</h2>
<p>Before proceeding, please check your email for a verification link.</p>
<p>If you did not receive the email</p>

<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
    @csrf
    <button type="submit" class="button__verifyEmail">{{ __('Click here to request another') }}</button>
</form>


@endsection
