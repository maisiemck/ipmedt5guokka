@extends('welcome')

@section('content')

<p>Wel ouder - overzichtspagina</p>

<table class="table">
  <caption class="table__caption">Riemdetectie</caption>
  <thead class="table__thead">
    <tr class="table__thead__tr">
      <th scope="column" class="table__thead__tr__th">Timestamp</td>
      <th scope="column" class="table__thead__tr__th">Hond</td>
      <th scope="column" class="table__thead__tr__th">Riemdetectie</td>
    </tr>
  </thead>
  <tbody class="table__tbody">
    <tr class="table__tbody__tr">
      <td class="table__tbody__tr__td" data-label="Timestamp">Testdate</th>
      <td class="table__tbody__tr__td" data-label="Hond">Bobby</td>
      <td class="table__tbody__tr__td" data-label="Riemdetectie">riemIn</td>
    </tr>

    @foreach($riemdetectie as $detectie)
    <tr class="table__tbody__tr">
      <td class="table__tbody__tr__td" data-label="Timestamp">{{$detectie->timestamp}}</th>
      <td class="table__tbody__tr__td" data-label="Hond">{{$detectie->hond}}</td>
      <td class="table__tbody__tr__td" data-label="Riemdetectie">{{$detectie->riemDetectie}}</td>
    </tr>
    @endforeach
  </tbody>
</table>

<table class="table">
  <caption class="table__caption">Waterdetectie</caption>
  <thead class="table__thead">
    <tr class="table__thead__tr">
      <th scope="column" class="table__thead__tr__th">Timestamp</td>
      <th scope="column" class="table__thead__tr__th">Hond</td>
      <th scope="column" class="table__thead__tr__th">Waterpeil</td>
    </tr>
  </thead>
  <tbody class="table__tbody">
    <tr class="table__tbody__tr">
      <td class="table__tbody__tr__td" data-label="Timestamp">Testdate</th>
      <td class="table__tbody__tr__td" data-label="Hond">Bobby</td>
      <td class="table__tbody__tr__td" data-label="Waterpeil">Peil iets</td>
    </tr>

    @foreach($peilConditie as $conditie)
      <tr class="table__tbody__tr">
        <td class="table__tbody__tr__td" data-label="Timestamp">{{$conditie->timeStamp}}</th>
        <td class="table__tbody__tr__td" data-label="Hond">{{$conditie->hond}}</td>
        <td class="table__tbody__tr__td" data-label="Waterpeil">{{$conditie->peilConditie}}</td>
      </tr>
    @endforeach

    @foreach($riemdetectie as $detectie)

    @endforeach
  </tbody>
</table>

<table class="table">
  <caption class="table__caption">Gewichtdetectie</caption>
  <thead class="table__thead">
    <tr class="table__thead__tr">
      <th scope="column" class="table__thead__tr__th">Timestamp</td>
      <th scope="column" class="table__thead__tr__th">Hond</td>
      <th scope="column" class="table__thead__tr__th">Hoeveelheid gram in bak</td>
    </tr>
  </thead>
  <tbody class="table__tbody">
    <tr class="table__tbody__tr">
      <td class="table__tbody__tr__td" data-label="Timestamp">Testdate</th>
      <td class="table__tbody__tr__td" data-label="Hond">Bobby</td>
      <td class="table__tbody__tr__td" data-label="HoeveelheidGramInBak">150 gram</td>
    </tr>

    @foreach($gewichtDetectie as $detectie)
      <tr class="table__tbody__tr">
        <td class="table__tbody__tr__td" data-label="Timestamp">{{$detectie->timeStamp}}</th>
        <td class="table__tbody__tr__td" data-label="Hond">{{$detectie->hond}}</td>
        <td class="table__tbody__tr__td" data-label="Waterpeil">{{$detectie->gewicht}}</td>
      </tr>
    @endforeach

    @foreach($riemdetectie as $detectie)

    @endforeach
  </tbody>
</table>

@endsection
