@extends('welcome')

@section('content')

  @if ($user->role == 'kind')
    @include('content.overzichtspagina.kind')
  @elseif ($user->role == 'ouder')
    @include('content.overzichtspagina.ouder')
  @else
    <p>Not yet part of a family are you? Go to settings to join or create one!</p>
  @endif
@endsection
