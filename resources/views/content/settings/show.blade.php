@extends('welcome')

@section('content')
  <h2>Settings</h2>

  <!-- Familie maken / beheren stuk -->
  @if ($user->familie == '0')
    @include ('content.settings.createfamilie')
    @include ('content.settings.joinfamilie')
  @else
    @switch($user->role)
        @case('kind')

            @break

        @case('ouder')


            @break

        @default
            <p>default</p>
    @endswitch

    @include ('content.settings.familieoverzicht')

    <!-- Beheren instellingen hond voor ouders -->
    @if ($hond->id == 't')
      @include ('content.settings.maakhond')
    @else
      @include ('content.settings.hond')
      @include ('content.settings.voertijden')
      @include ('content.settings.medicatieoverzicht')
      @include ('content.settings.medicatietoevoegen')
      @include ('content.settings.wandelingoverzicht')
      @include ('content.settings.wandelingtoevoegen')
    @endif
    @include ('content.settings.vorigehonden')
  @endif


  <!-- Beheren instellingen voor account -->

  <h2>Account gegevens</h2>
  <p>Note: email can't be changed as only rol is saved in the SettingsController update function</p>
  <p>TODO</p>
  <ul>
    <li>Email updaten</li>
    <li>Naam updaten</li>
  </ul>
  <form class="formulier" action="/settings/{{ Auth::user()->email }}/update" method="post">
    @csrf
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <label class="formulier__label" for="email">Email:</label>
    <input class="formulier__input" type="text" name="email" value="{{ Auth::user()->email }}"></br>

    <label class="formulier__label" for="rol">Rol: </label>
    <select class="formulier__input"  name="rol">
    @foreach ($roles as $role)
      <option value="{{$role->name}}">{{$role->name}}</option>
    @endforeach
    </select></br>

    <button class="button__groot" type="submit" name="button">Update info</button>
  </form>
@endsection
